<?php


namespace App\Services;


interface FoursquareApiServiceInterface
{
    /**
     * @return mixed
     */
    public function getCategories();

    /**
     * @param array $params
     * @return mixed
     */
    public function getVenuesByCategory(array $params);

    /**
     * @param string $venueId
     * @return mixed
     */
    public function getVenueImages(string $venueId);

}