<?php


namespace App\Services;

use GuzzleHttp\Client;


class GuzzleFoursquareApiService implements FoursquareApiServiceInterface
{
    private $clientId;
    private $secretId;
    private $url;
    private $version;
    private $guzzleService;

    public function __construct(string $clientId, string $secretId, string $url, string $version, Client $guzzleService)
    {
        $this->clientId = $clientId;
        $this->secretId = $secretId;
        $this->url = $url;
        $this->version = $version;
        $this->guzzleService = $guzzleService;
    }

    /**
     * @return mixed
     */
    public function getCategories()
    {
        $categoriesUrl = "$this->url/categories?v=$this->version&client_id=$this->clientId&client_secret=$this->secretId";

        $result = $this->guzzleService->request('GET', $categoriesUrl);

        $categories = json_decode($result->getBody()->getContents())->response->categories;

        return $categories;
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function getVenuesByCategory(array $params)
    {
        $venuesUrl = "$this->url/search?v=$this->version&client_id=$this->clientId&client_secret=$this->secretId";

        foreach ($params as $key => $value) {
            $venuesUrl .= "&$key=$value";
        }

        $result = $this->guzzleService->request('GET', $venuesUrl);

        $venues = json_decode($result->getBody()->getContents())->response->venues;

        return $venues;
    }

    /**
     * @param string $venueId
     * @return mixed
     */
    public function getVenueImages(string $venueId)
    {
        $venuesUrl = "$this->url/$venueId/photos?v=$this->version&client_id=$this->clientId&client_secret=$this->secretId";

        $result = $this->guzzleService->request('GET', $venuesUrl);

        $photos = json_decode($result->getBody()->getContents())->response->photos->items;

        return $photos;
    }
}