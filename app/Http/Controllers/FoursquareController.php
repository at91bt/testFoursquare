<?php


namespace App\Http\Controllers;


use App\Services\FoursquareApiServiceInterface;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class FoursquareController extends Controller
{
    private $foursquareApi;

    public function __construct(FoursquareApiServiceInterface $foursquareApiService)
    {
        $this->foursquareApi = $foursquareApiService;
    }

    /**
     *
     * @param Request $request
     * @return Factory|\Illuminate\View\View
     */
    public function get(Request $request)
    {
        $categories = $this->foursquareApi->getCategories();
        $venues = [];
        $category = $request->get('category');

        if ($category) {
            $venues = $this->foursquareApi->getVenuesByCategory([
                'near'       => 'Valetta',
                'categoryId' => $category,
            ]);
        }
        $selectedCategory = $category;

        return view('welcome', compact('categories', 'venues', 'selectedCategory'));
    }

    /**
     * @param $venueId
     * @return string
     */
    public function getVenueImages($venueId)
    {
        $photos = $this->foursquareApi->getVenueImages($venueId);
        if (count($photos)) {
            $firstPhoto = $photos[0];
            $photoUrl = $firstPhoto->prefix . '400x300' . $firstPhoto->suffix;
        } else {
            $photoUrl = 'https://dummyimage.com/400x300/858585/07070a.png&text=No+image+available';
        }

        $img = Image::make(file_get_contents($photoUrl));

        return $img->response('png', 70);
    }
}