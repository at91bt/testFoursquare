<?php


namespace App\Providers;


use App\Services\FoursquareApiServiceInterface;
use App\Services\GuzzleFoursquareApiService;
use GuzzleHttp\Client;
use Illuminate\Support\ServiceProvider;

class FoursquareServiceProvider extends ServiceProvider
{
    /**
     *
     */
    public function register()
    {
        $this->app->bind(FoursquareApiServiceInterface::class, function () {
            return new GuzzleFoursquareApiService(
                env('FOURSQUARE_CLIENT_ID'),
                env('FOURSQUARE_SECRET_ID'),
                env('FOURSQUARE_URL'),
                env('FOURSQUARE_API_VERSION'),
                new Client());
        });
    }

}