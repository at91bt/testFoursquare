@if(count($category->categories) > 0)

    <li data-toggle="collapse" data-target="{{'#'.$category->id}}"
        class="collapsed  {{$category->id === request()->get('category') ? 'active' : ''}}" aria-expanded="true">
        <a href="#">
            <img src="{{$category->icon->prefix.'bg_32'.$category->icon->suffix}}"/>
            {{$category->name}}
            <span class="arrow"></span></a>
    </li>
    <ul class="sub-menu collapse" id="{{$category->id}}">
        @each('template', $category->categories, 'category')
    </ul>

@else
    <li class="{{$category->id === request()->get('category') ? 'active' : ''}}">
        <a href="{{route('venues', ['category' => $category->id])}}"><img
                    src="{{$category->icon->prefix.'bg_32'.$category->icon->suffix}}"/> {{$category->name}}
        </a>
    </li>
@endif